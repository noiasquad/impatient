/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package ch.obya.education.android.impatient.repository;


import ch.obya.education.android.impatient.domain.model.MedicalRecord;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author olivier
 */
@Component
public class PatientFileManager {

	public static PatientFileManager get() throws IOException {
		return new PatientFileManager();
	}

	private Path targetDir_ = Paths.get("photos");

	private PatientFileManager() throws IOException{
		if(!Files.exists(targetDir_)){
			Files.createDirectories(targetDir_);
		}
	}

	private Path getPhotoPath(MedicalRecord record) {
		assert(record != null);
		return targetDir_.resolve("patient"+record.getId()+".jpg");
	}

	public boolean hasPhotoData(MedicalRecord record) {
		Path source = getPhotoPath(record);
		return Files.exists(source);
	}

	public void copyPhotoData(MedicalRecord record, OutputStream out) throws IOException {
		Path source = getPhotoPath(record);
		if(!Files.exists(source)){
			throw new FileNotFoundException("Unable to find the referenced image file for patient identifier:" + record.getId());
		}
		Files.copy(source, out);
	}

	public void savePhotoData(MedicalRecord record, InputStream videoData) throws IOException{
		assert(videoData != null);
		Path target = getPhotoPath(record);
		Files.copy(videoData, target, StandardCopyOption.REPLACE_EXISTING);
	}
	
}
