package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Treatment;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by olivier on 28.10.15.
 */
@Component
public class TreatmentSetImpl implements TreatmentSet, TreatmentTimeEstimator {

    private LinkedList<String> waitingList = new LinkedList<String>();

    private List<String> candidatesList = new ArrayList<String>();

    private Map<String, Treatment> index = new HashMap<String,Treatment>();

    private Map<String, String> patients = new HashMap<String,String>();

    @Override
    public Treatment remove(String id) {
        Treatment treatment = index.get(id);
        waitingList.remove(id);
        candidatesList.remove(id);
        index.remove(id);
        patients.remove(treatment.getPatientId());
        return treatment;
    }

    @Override
    public Treatment shift(String id, int rankCount) {
        int pos, size;
        if (rankCount != 0 && (pos = waitingList.indexOf(id)) >= 0 && (size = waitingList.size()) > 1) {
            waitingList.remove(pos);
            waitingList.add(Math.max(Math.min(pos + rankCount, size),0), id);
        }
        return null;
    }

    @Override
    public Treatment toFirst(String id) {
        int pos;
        if ((pos = waitingList.indexOf(id)) > 0) {
            waitingList.remove(pos);
            waitingList.addFirst(id);
        }
        return index.get(id);
    }

    @Override
    public Treatment toLast(String id) {
        int pos;
        if ((pos = waitingList.indexOf(id)) != waitingList.size()-1) {
            waitingList.remove(pos);
            waitingList.addLast(id);
        }
        return index.get(id);
    }

    @Override
    public Treatment toRank(String id, int rankId) {
        int pos, size;
        if ((pos = waitingList.indexOf(id)) >= 0 && (rankId != pos) && (size = waitingList.size()) > 1) {
            if (0 <= rankId && rankId < size) {
                waitingList.remove(pos);
                waitingList.add(rankId, id);
            }
        }
        return index.get(id);
    }

    @Override
    public Treatment add(Treatment treatment) {
        waitingList.add(treatment.getId());
        index.put(treatment.getId(), treatment);
        patients.put(treatment.getPatientId(), treatment.getId());
        return treatment;
    }

    @Override
    public Treatment first() {
        return index.get(waitingList.getFirst());
    }

    @Override
    public Treatment promoteNext() {
        String id;
        candidatesList.add(id = waitingList.removeFirst());
        return index.get(id);
    }

    @Override
    public Treatment set(Treatment treatment) {
        index.put(treatment.getId(), treatment);
        return treatment;
    }

    @Override
    public Treatment get(String id) {
        return index.get(id);
    }

    @Override
    public Treatment getByPatient(String patientId) {
        return get(patients.get(patientId));
    }

    @Override
    public List<Treatment> elements() {
        List<Treatment> result = new ArrayList<Treatment>();
        for (String id : candidatesList) {
            result.add(index.get(id));
        }
        for (String id : waitingList) {
            result.add(index.get(id));
        }
        return result;
    }

    @Override
    public List<Treatment> candidates() {
        List<Treatment> result = new ArrayList<Treatment>();
        for (String id : candidatesList) {
            result.add(index.get(id));
        }
        return result;
    }

    @Override
    public Treatment adjustEstimate(Treatment treatment) {
        return null;
    }

    @Override
    public List<Treatment> adjustEstimates() {
        return null;
    }
}
