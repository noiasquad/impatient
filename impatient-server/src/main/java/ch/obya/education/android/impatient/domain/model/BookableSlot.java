package ch.obya.education.android.impatient.domain.model;

import java.util.Date;

/**
 * Created by olivier on 05.11.15.
 */
public class BookableSlot {
    private Date day;
    private Byte slotId;
    private Date scheduleTime;
    
	public BookableSlot() {
	}

	public BookableSlot(Date day, Byte slotId, Date scheduleTime) {
		this.day = day;
		this.slotId = slotId;
		this.scheduleTime = scheduleTime;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Byte getSlotId() {
		return slotId;
	}

	public void setSlotId(Byte slotId) {
		this.slotId = slotId;
	}

	public Date getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(Date scheduleTime) {
		this.scheduleTime = scheduleTime;
	}
}
