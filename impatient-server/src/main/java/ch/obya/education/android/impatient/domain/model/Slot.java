package ch.obya.education.android.impatient.domain.model;


/**
 * Created by olivier on 05.11.15.
 */
public class Slot extends BookableSlot {
	private String reservationId;
    private String patientId;
    
	public Slot() {
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}
	
	public String getPatientId() {
		return patientId;
	}
	
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
    
    
}
