package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Treatment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by olivier on 26.10.15.
 */
@Component
public class TreatmentRoomImpl implements TreatmentRoom {

    @Value("${treatmentDurationMs}")
    private long treatmentDurationMs;
    @Value("${closeToEndNotificationTimeMs}")
    private long closeToEndNotificationTimeMs;

    @Autowired
    private MessageBroker messageBroker;
    @Autowired
    private TreatmentLifecycle treatmentLifecycle;

    private AtomicInteger freeMachinesCount;
    
    public TreatmentRoomImpl() {
	}

	public TreatmentRoomImpl(@Value("${machinesCount}") int machinesCount) {
        freeMachinesCount.set(machinesCount);
    }

    public Treatment verify(final Treatment treatment) {
        treatmentLifecycle.assertVerify(treatment);

        if (freeMachinesCount.get() > 0) {
            new Thread(new MachineJob(treatmentLifecycle.verify(treatment))).start();
        }
        return treatment;
    }

    private class MachineJob implements Runnable {
        private Treatment treatment;

        private MachineJob(Treatment treatment) {
            this.treatment = treatment;
        }

        @Override
        public void run() {
            try {
                treatment = treatmentLifecycle.treat(treatment);
                Thread.sleep(treatmentDurationMs - closeToEndNotificationTimeMs);
                messageBroker.publish(TREATMENT_ROOM_TOPIC, createMessage("TREATMENT_CLOSE_TO_END", treatment));
                Thread.sleep(closeToEndNotificationTimeMs);
            } catch (InterruptedException e) {
            } finally {
                treatment = treatmentLifecycle.end(treatment);
                messageBroker.publish(TREATMENT_ROOM_TOPIC, createMessage("TREATMENT_TERMINATED", treatment));
                freeMachinesCount.incrementAndGet();
            }
        }
    }

    private static Map<String,String> createMessage(String label, Treatment treatment) {
        Map<String,String> message = new HashMap<String, String>();
        message.put(label, treatment.getId());
        message.put("PATIENT_ID", treatment.getPatientId());
        return message;
    }
}
