package ch.obya.education.android.impatient.repository;

import ch.obya.education.android.impatient.domain.model.Reservation;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by olivier on 26.10.15.
 */
public interface ReservationRepository extends CrudRepository<Reservation, String> {

    List<Reservation> findByScheduleTime(Date scheduleTime);
    Reservation findByTreatmentId(String id);
    List<Reservation> findByPatientId(String id);
}
