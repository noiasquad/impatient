package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Treatment;

/**
 * Created by olivier on 11.11.15.
 */
public interface TreatmentLifecycle {
    Treatment check(final Treatment treatment) throws LifecycleException;
    Treatment promote(final Treatment treatment) throws LifecycleException;
    Treatment verify(final Treatment treatment) throws LifecycleException;
    Treatment treat(final Treatment treatment) throws LifecycleException;
    Treatment end(final Treatment treatment) throws LifecycleException;
    Treatment cancel(final Treatment treatment) throws LifecycleException;

    void assertCheck(final Treatment treatment) throws LifecycleException;
    void assertPromote(final Treatment treatment) throws LifecycleException;
    void assertVerify(final Treatment treatment) throws LifecycleException;
    void assertTreat(final Treatment treatment) throws LifecycleException;
    void assertEnd(final Treatment treatment) throws LifecycleException;
    void assertCancel(final Treatment treatment) throws LifecycleException;
    void assertDelay(final Treatment treatment) throws LifecycleException;
    void assertReschedule(Treatment treatment) throws LifecycleException;
}
