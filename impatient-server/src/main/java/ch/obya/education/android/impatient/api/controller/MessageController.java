package ch.obya.education.android.impatient.api.controller;

import static ch.obya.education.android.impatient.api.MessageApi.CURRENT_USER_MESSAGES_PATH;
import static ch.obya.education.android.impatient.api.MessageApi.MESSAGE_ENDPOINT_PATH;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import ch.obya.education.android.impatient.adapter.ServerSendEventAdapter;
import ch.obya.education.android.impatient.domain.logic.MessageBroker;
import ch.obya.education.android.impatient.security.Authorities;

/**
 * Created by olivier on 24.10.15.
 */
@Controller
@RequestMapping(value = MESSAGE_ENDPOINT_PATH)
public class MessageController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private MessageBroker messageBroker;

    @Secured(Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_MESSAGES_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Map<String,String>> consume(Principal principal) {
        List<Map<String,String>> messages = messageBroker.consume(principal.getName());
        if (messages == null) throw new ResourceNotFoundException(principal.getName());
        return messages;
    }

    @Secured(Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_MESSAGES_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    SseEmitter subscribe(Principal principal) {
        SseEmitter sseEmitter = new SseEmitter();
        messageBroker.subscribe(principal.getName(), new ServerSendEventAdapter(sseEmitter));
        return sseEmitter;
    }
}
