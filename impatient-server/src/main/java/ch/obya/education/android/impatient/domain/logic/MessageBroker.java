package ch.obya.education.android.impatient.domain.logic;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Multimaps;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by olivier on 28.10.15.
 */
@Component
public class MessageBroker {

    private ListMultimap<String, Map<String,String>> topics =
            MultimapBuilder.<Map<String,String>>linkedHashKeys().arrayListValues().build();

    private ListMultimap<String, Map<String,String>> syncTopics =
            Multimaps.<String,Map<String,String>>synchronizedListMultimap(topics);

    private ListMultimap<String, Consumer> consumers = MultimapBuilder.<String,Consumer>linkedHashKeys().arrayListValues().build();
    private ListMultimap<String, Consumer> syncConsumers =Multimaps.<String,Consumer>synchronizedListMultimap(consumers);

    public void publish(String topic, Map<String,String> message) {
        syncTopics.put(topic, message);
    }

    public void subscribe(String topic, Consumer consumer) {
        syncConsumers.put(topic, consumer);
    }

    public List<Map<String,String>> consume(String topic) {
        return syncTopics.removeAll(topic);
    }

    public interface Consumer {
        void consume(List<Map<String,String>> messages);
    }

    public void dispatch() {
        for (String topic : syncTopics.keySet()) {
            for (Consumer consumer : syncConsumers.get(topic)) {
                consumer.consume(syncTopics.removeAll(topic));
            }
        }
    }
}
