package ch.obya.education.android.impatient.domain.logic;

import java.util.Date;
import java.util.List;

import ch.obya.education.android.impatient.domain.model.BookableSlot;
import ch.obya.education.android.impatient.domain.model.Slot;

public interface Agenda {
	List<BookableSlot> freeSlots();
	List<Slot> today();
	List<Slot> inDay(Date day);
	List<Slot> thisWeek();
	List<Slot> inWeek(Date dayInWeek);
	List<Slot> thisMonth();
	List<Slot> inMonth(Date dayInMonth);
	Date scheduleTime(Date day, Byte slotId);
}
