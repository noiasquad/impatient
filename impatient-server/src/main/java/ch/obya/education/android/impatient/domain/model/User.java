package ch.obya.education.android.impatient.domain.model;

/**
 * Created by olivier on 15.11.15.
 */
public class User {
    private String id;
    private String secret;

    public User(String id, String secret) {
        this.id = id;
        this.secret = secret;
    }

    public String getId() {
        return id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
