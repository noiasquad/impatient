package ch.obya.education.android.impatient.api.controller;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ch.obya.education.android.impatient.domain.logic.LifecycleException;

/**
 * Created by olivier on 26.10.15.
 */
public abstract class AbstractController {

    @ResponseStatus(value = BAD_REQUEST)
    public class BadRequestException extends RuntimeException {
        public BadRequestException(String operation) {
            super(String.format("operation not available at the moment", operation));
        }
    }

    @ResponseStatus(value = NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
        public ResourceNotFoundException(String patientId) {
            super(String.format("no record with id %d found", patientId));
        }
    }

    @ResponseStatus(value = UNAUTHORIZED)
    public class UnauthorizedResourceException extends RuntimeException {
        public UnauthorizedResourceException(String userId, String patientId) {
            super(String.format("current authenticated user %s is not authorized to access resource with id %d", userId, patientId));
        }
    }

    @ResponseStatus(value = FORBIDDEN)
    public class ForbiddenResourceException extends RuntimeException {
        public ForbiddenResourceException(String userId, String patientId) {
            super(String.format("current authenticated user %s is not authorized to access resource with id %d", userId, patientId));
        }
    }

    @ExceptionHandler(LifecycleException.class)
    public void handleLifecycleException(LifecycleException ex, HttpServletRequest request) {
        throw new BadRequestException(ex.getLocalizedMessage());
    }

    @ResponseStatus(value = INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IOException.class)
    public @ResponseBody
    ErrorInfo handleIOException(Exception ex, HttpServletRequest request) {
        return new ErrorInfo(request.getRequestURL().toString(), ex);
    }

    public class ErrorInfo {
        public final String url;
        public final String ex;

        public ErrorInfo(String url, Exception ex) {
            this.url = url;
            this.ex = ex.getLocalizedMessage();
        }
    }
}
