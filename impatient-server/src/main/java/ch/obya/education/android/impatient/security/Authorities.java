package ch.obya.education.android.impatient.security;

/**
 * Created by olivier on 26.10.15.
 */
public interface Authorities {
    String ROLE_CLIENT = "ROLE_CLIENT";
    String ROLE_ADMIN = "ROLE_ADMIN";
    String ROLE_PATIENT = "ROLE_PATIENT";
}
