package ch.obya.education.android.impatient.adapter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import ch.obya.education.android.impatient.domain.logic.MessageBroker;

/**
 * Created by olivier on 28.10.15.
 */
public class ServerSendEventAdapter implements MessageBroker.Consumer {

    private final SseEmitter sseEmitter;

    public ServerSendEventAdapter(SseEmitter sseEmitter) {
        this.sseEmitter = sseEmitter;
    }

    @Override
    public void consume(List<Map<String, String>> messages) {
        try {
            sseEmitter.send(sseEmitter.event().data(messages, MediaType.APPLICATION_JSON));
            sseEmitter.complete();
        } catch (IOException e) {
            sseEmitter.completeWithError(e);
        }
    }
}
