package ch.obya.education.android.impatient.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;

/**
 * Configure this web application to use OAuth 2.0.
 * <p>
 * The resource server is located at "/video", and can be accessed only by retrieving a token from "/oauth/token"
 * using the Password Grant Flow as specified by OAuth 2.0.
 * <p>
 * Most of this code can be reused in other applications. The key methods that would definitely need to
 * be changed are:
 * <p>
 * ResourceServer.configure(...) - update this method to apply the appropriate
 * set of scope requirements on client requests
 * <p>
 * OAuth2Config constructor - update this constructor to create a "real" (not hard-coded) UserDetailsService
 * and ClientDetailsService for authentication. The current implementation should never be used in any
 * type of production environment as these hard-coded credentials are highly insecure.
 */
@Configuration
public class OAuth2SecurityConfiguration {

   /**
     * This method is used to configure who is allowed to access which parts of our
     * resource server (i.e. the "/video" endpoint)
     */
    @Configuration
    @EnableResourceServer
    protected static class ResourceServer extends ResourceServerConfigurerAdapter {

        // This method configures the OAuth scopes required by clients to access
        // all of the paths in the video service.
        @Override
        public void configure(HttpSecurity http) throws Exception {

            http.csrf().disable();

            http
                    .authorizeRequests()
                    .antMatchers("/oauth/token").anonymous();


            // If you were going to reuse this class in another
            // application, this is one of the key sections that you
            // would want to change

            // Require all GET requests to have client "read" scope
            http
                    .authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/**")
                    .access("#oauth2.hasScope('read')");

            // Require all other requests to have "write" scope
            http
                    .authorizeRequests()
                    .antMatchers("/**")
                    .access("#oauth2.hasScope('write')");
        }
    }

    /**
     * This class is used to configure how our authorization server (the "/oauth/token" endpoint)
     * validates client credentials.
     */
    @Configuration
    @EnableAuthorizationServer
    protected static class OAuth2Config extends AuthorizationServerConfigurerAdapter {

        // Delegate the processing of Authentication requests to the framework
        @Autowired
        @Qualifier("authenticationManagerBean")
        private AuthenticationManager authenticationManager;

        /**
         * Return the list of trusted client information to anyone who asks for it.
         */
        @Bean
        public ClientDetailsService clientDetailsService() throws Exception {
            return new InMemoryClientDetailsServiceBuilder()
                    .withClient("mobile").authorizedGrantTypes("password")
                    .authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT")
                    .scopes("read", "write").resourceIds("impatient")
                    .accessTokenValiditySeconds(3600).and().build();
        }

        /**
         * Return all of our user information to anyone in the framework who requests it.
         */
        @Bean
        public UserDetailsService userDetailsService() {
            return new InMemoryUserDetailsManager(
                Arrays.asList(
                    User.create("admin", "admin", Authorities.ROLE_ADMIN),
                    User.create("doctor", "admin", Authorities.ROLE_ADMIN),
                    User.create("reception", "admin", Authorities.ROLE_ADMIN),
                    User.create("nurse", "admin", Authorities.ROLE_ADMIN),
                    User.create("olivier", "o", Authorities.ROLE_PATIENT),
                    User.create("patient1", "p", Authorities.ROLE_PATIENT),
                    User.create("patient2", "p", Authorities.ROLE_PATIENT),
                    User.create("patient3", "p", Authorities.ROLE_PATIENT),
                    User.create("patient4", "p", Authorities.ROLE_PATIENT),
                    User.create("patient5", "p", Authorities.ROLE_PATIENT)));
        }

        /**
         * This method tells our AuthorizationServerConfigurerAdapter to use the delegated AuthenticationManager
         * to process authentication requests.
         */
        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.authenticationManager(authenticationManager);
        }

        /**
         * This method tells the AuthorizationServerConfigurerAdapter to use our self-defined client details service to
         * authenticate clients with.
         */
        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.withClientDetails(clientDetailsService());
        }

    }
}
