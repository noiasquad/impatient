package ch.obya.education.android.impatient.api.controller;

import static ch.obya.education.android.impatient.api.UserApi.ALL_USERS_PATH;
import static ch.obya.education.android.impatient.api.UserApi.CURRENT_USER_ID_PATH;
import static ch.obya.education.android.impatient.api.UserApi.CURRENT_USER_SECRET_PATH;
import static ch.obya.education.android.impatient.api.UserApi.USER_ENDPOINT_PATH;
import static ch.obya.education.android.impatient.api.UserApi.USER_ID;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ch.obya.education.android.impatient.security.SecretGenerator;

/**
 * Created by olivier on 24.10.15.
 */
@Controller
@RequestMapping(value = USER_ENDPOINT_PATH)
public class UserController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value= ALL_USERS_PATH, method= RequestMethod.POST)
    public @ResponseBody
    String[] createUser(@PathVariable(USER_ID) String userId) {
        return new String[] { userId, SecretGenerator.generate() };
    }

    @RequestMapping(value= CURRENT_USER_ID_PATH, method= RequestMethod.GET)
    public @ResponseBody
    String getCurrentUser(Principal principal) {
        return principal.getName();
    }

    @RequestMapping(value= CURRENT_USER_SECRET_PATH, method= RequestMethod.POST)
    public @ResponseBody
    void changeSecret(String oldSecret, String newSecret, Principal principal) {
        throw new ForbiddenResourceException(principal.getName(), "modify secret");
    }
}
