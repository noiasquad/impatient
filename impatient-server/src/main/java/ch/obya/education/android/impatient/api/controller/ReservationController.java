package ch.obya.education.android.impatient.api.controller;

import static ch.obya.education.android.impatient.api.ReservationApi.*;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ch.obya.education.android.impatient.domain.logic.Agenda;
import ch.obya.education.android.impatient.domain.logic.WaitingRoom;
import ch.obya.education.android.impatient.domain.model.BookableSlot;
import ch.obya.education.android.impatient.domain.model.Reservation;
import ch.obya.education.android.impatient.domain.model.Slot;
import ch.obya.education.android.impatient.repository.PatientRepository;
import ch.obya.education.android.impatient.repository.ReservationRepository;
import ch.obya.education.android.impatient.security.Authorities;

/**
 * Created by olivier on 24.10.15.
 */
@Controller
@RequestMapping(RESERVATION_ENDPOINT_PATH)
public class ReservationController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(ReservationController.class);

    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private WaitingRoom waitingRoom;
    @Autowired
    private Agenda agenda;

    @Secured(Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_RESERVATIONS_PATH, method = RequestMethod.GET)
    public List<Reservation> getCurrentUserReservations(Principal principal) throws IOException {
        return reservationRepository.findByPatientId(principal.getName());
    }

    @Secured({Authorities.ROLE_ADMIN, Authorities.ROLE_PATIENT})
    @RequestMapping(value = FREE_SLOTS_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<BookableSlot> getFreeSlots() {
        return agenda.freeSlots();
    }

    @Secured (Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_RESERVE_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    Reservation createReservation(@PathVariable(DAY_ID) Date day, @PathVariable(SLOT_ID) Byte slotId, Principal principal) {
        return reservationRepository.save(new Reservation(agenda.scheduleTime(day, slotId), day, slotId, principal.getName()));
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = TODAY_SLOTS_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Slot> getTodayAgenda() {
        return agenda.today();
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = SLOTS_IN_DAY_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Slot> getDayAgenda(@PathVariable(DAY_ID) Date day) {
        return agenda.inDay(day);
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = THIS_WEEK_SLOTS_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Slot> getWeekAgenda() {
        return agenda.thisWeek();
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = SLOTS_IN_WEEK_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Slot> getWeekAgenda(@PathVariable(DAY_ID) Date dateInWeek) {
        return agenda.inWeek(dateInWeek);
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = THIS_MONTH_SLOTS_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Slot> getMonthAgenda() {
        return agenda.thisMonth();
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = SLOTS_IN_MONTH_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Slot> getMonthAgenda(@PathVariable(DAY_ID) Date dateInMonth) {
        return agenda.inMonth(dateInMonth);
    }

    @Secured({Authorities.ROLE_ADMIN, Authorities.ROLE_PATIENT})
    @RequestMapping(value = CHECK_IN_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    String checkIn(@PathVariable(RESERVATION_ID) String reservationId,
                 HttpServletRequest request,
                 Principal principal) {
        Reservation reservation = load(reservationId);
        accessControl(reservation, request, principal);
        reservation.setTreatmentId(waitingRoom.checkIn(reservation).getPatientId());
        reservationRepository.save(reservation);
        return reservation.getTreatmentId();
    }

    @Secured({Authorities.ROLE_ADMIN, Authorities.ROLE_PATIENT})
    @RequestMapping(value = ON_RESERVATION_PATH, method = RequestMethod.DELETE)
    public
    @ResponseBody
    void cancel(@PathVariable(RESERVATION_ID) String reservationId,
                HttpServletRequest request,
                Principal principal) {
        Reservation reservation = load(reservationId);
        accessControl(reservation, request, principal);
        reservationRepository.delete(reservationId);
    }

    private void accessControl(Reservation reservation, HttpServletRequest request, Principal principal) {
        if (!request.isUserInRole(Authorities.ROLE_ADMIN) && !principal.getName().equals(reservation.getPatientId()))
            throw new UnauthorizedResourceException(principal.getName(), reservation.getId().toString());
    }

    private Reservation load(String reservationId) throws ResourceNotFoundException {
        Reservation reservation = reservationRepository.findOne(reservationId);
        if (reservation != null) {
            return reservation;
        }
        else throw new ResourceNotFoundException(reservationId);
    }
}
