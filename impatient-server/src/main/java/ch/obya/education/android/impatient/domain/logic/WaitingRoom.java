package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Reservation;
import ch.obya.education.android.impatient.domain.model.Treatment;

/**
 * Created by olivier on 26.10.15.
 */
public interface WaitingRoom {

    public enum Reschedule {
        TO_FIRST, TO_LAST, SHIFT
    }

    public Treatment checkIn(Reservation reservation);

    public Treatment delay(Treatment treatment, int minutes);

    public Treatment reschedule(Treatment treatment, Reschedule reschedule, int rankCount);

    public void cancel(Treatment treatment);
}

