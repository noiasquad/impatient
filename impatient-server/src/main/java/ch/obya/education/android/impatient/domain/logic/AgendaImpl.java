package ch.obya.education.android.impatient.domain.logic;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ch.obya.education.android.impatient.domain.model.BookableSlot;
import ch.obya.education.android.impatient.domain.model.Slot;
import ch.obya.education.android.impatient.repository.ReservationRepository;

@Component
public class AgendaImpl implements Agenda {

	@Autowired
	private ReservationRepository reservationRepository;

	@Override
	public List<BookableSlot> freeSlots() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Slot> today() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Slot> inDay(Date day) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Slot> thisWeek() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Slot> inWeek(Date dayInWeek) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Slot> thisMonth() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Slot> inMonth(Date dayInMonth) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date scheduleTime(Date day, Byte slotId) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
