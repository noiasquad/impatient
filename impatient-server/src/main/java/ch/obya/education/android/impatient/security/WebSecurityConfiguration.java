package ch.obya.education.android.impatient.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Configure this web application to use OAuth 2.0.
 * <p>
 * The resource server is located at "/video", and can be accessed only by retrieving a token from "/oauth/token"
 * using the Password Grant Flow as specified by OAuth 2.0.
 * <p>
 * Most of this code can be reused in other applications. The key methods that would definitely need to
 * be changed are:
 * <p>
 * ResourceServer.configure(...) - update this method to apply the appropriate
 * set of scope requirements on client requests
 * <p>
 * OAuth2Config constructor - update this constructor to create a "real" (not hard-coded) UserDetailsService
 * and ClientDetailsService for authentication. The current implementation should never be used in any
 * type of production environment as these hard-coded credentials are highly insecure.
 * <p>
 * OAuth2SecurityConfiguration.containerCustomizer(...) - update this method to use a real keystore
 * and certificate signed by a CA. This current version is highly insecure.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
