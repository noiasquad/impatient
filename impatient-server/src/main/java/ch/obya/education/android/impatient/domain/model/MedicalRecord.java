package ch.obya.education.android.impatient.domain.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by olivier on 24.10.15.
 */
@Entity
public class MedicalRecord {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String address;
    private Date birthDate;

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
