package ch.obya.education.android.impatient.domain.model;

import java.util.Date;
import java.util.UUID;

/**
 * Created by olivier on 24.10.15.
 */
public class Treatment {

    public enum Status { NONE, WAITING, CANDIDATE, VERIFIED, IN_TREATMENT, TERMINATED };

    private String id;
    private Date scheduleTime;
    private String patientId;
    private Status status;

    public Treatment(String patientId) {
        this(null, patientId, Status.NONE);
    }

    public Treatment(Date scheduleTime, String patientId, Status status) {
        this.id = UUID.randomUUID().toString();
        this.scheduleTime = scheduleTime;
        this.patientId = patientId;
        this.status = status;
    }

    public Treatment modify(Date scheduleTime, Status status) {
        Treatment treatment = new Treatment(scheduleTime, getPatientId(), status);
        treatment.setId(getId());
        return treatment;
    }

    public Treatment modify(Status status) {
        return modify(getScheduleTime(), status);
    }

    public Treatment modify(Date scheduleTime) {
        return modify(scheduleTime, getStatus());
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public Date getScheduleTime() {
        return scheduleTime;
    }

    public String getPatientId() {
        return patientId;
    }

    public Status getStatus() {
        return status;
    }
}
