package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Treatment;

import java.util.List;

/**
 * Created by olivier on 28.10.15.
 */
public interface TreatmentSet {

    Treatment add(Treatment treatment);

    Treatment first();

    Treatment promoteNext();

    Treatment set(Treatment treatment);

    Treatment get(String id);

    Treatment getByPatient(String patientId);

    List<Treatment> elements();

    List<Treatment> candidates();

    Treatment remove(String id);

    Treatment shift(String id, int rankCount);

    Treatment toFirst(String id);

    Treatment toLast(String id);

    Treatment toRank(String id, int rankId);
}
