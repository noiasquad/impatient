package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Treatment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by olivier on 11.11.15.
 */
@Component
public class TreatmentLifecycleImpl implements TreatmentLifecycle {

    @Autowired
    private TreatmentSet treatments;

    @Override
    public Treatment check(Treatment treatment) throws LifecycleException {
        assertCheck(treatment);
        return treatments.add(treatment.modify(Treatment.Status.WAITING));
    }

    @Override
    public Treatment promote(Treatment treatment) throws LifecycleException {
        assertPromote(treatment);
        return treatments.set(treatment.modify(Treatment.Status.CANDIDATE));
    }

    @Override
    public Treatment verify(Treatment treatment) throws LifecycleException {
        assertVerify(treatment);
        return treatments.set(treatment.modify(Treatment.Status.VERIFIED));
    }

    @Override
    public Treatment treat(Treatment treatment) throws LifecycleException {
        assertEnd(treatment);
        return treatments.set(treatment.modify(new Date(), Treatment.Status.IN_TREATMENT));
    }

    @Override
    public Treatment end(Treatment treatment) throws LifecycleException {
        assertEnd(treatment);
        return treatments.remove(treatment.modify(Treatment.Status.TERMINATED).getId());
    }

    @Override
    public Treatment cancel(Treatment treatment) throws LifecycleException {
        assertCancel(treatment);
        return treatments.remove(treatment.getId());
    }

    @Override
    public void assertCheck(Treatment treatment) throws LifecycleException {
        if (treatment.getStatus().ordinal() >= Treatment.Status.WAITING.ordinal())
            throw new LifecycleException();
    }

    @Override
    public void assertPromote(Treatment treatment) throws LifecycleException {
        if (treatment.getStatus().ordinal() != Treatment.Status.WAITING.ordinal())
            throw new LifecycleException();
    }

    @Override
    public void assertVerify(Treatment treatment) throws LifecycleException {
        if (treatment.getStatus().ordinal() != Treatment.Status.CANDIDATE.ordinal())
            throw new LifecycleException();
    }

    @Override
    public void assertTreat(Treatment treatment) throws LifecycleException {
        if (treatment.getStatus().ordinal() != Treatment.Status.VERIFIED.ordinal())
            throw new LifecycleException();
    }

    @Override
    public void assertEnd(Treatment treatment) throws LifecycleException {
        if (treatment.getStatus().ordinal() < Treatment.Status.IN_TREATMENT.ordinal())
            throw new LifecycleException();
    }

    @Override
    public void assertCancel(Treatment treatment) throws LifecycleException {
        if (treatment.getStatus().ordinal() < Treatment.Status.WAITING.ordinal())
            throw new LifecycleException();
        if (treatment.getStatus().ordinal() > Treatment.Status.CANDIDATE.ordinal())
            throw new LifecycleException();
    }

    @Override
    public void assertDelay(Treatment treatment) throws LifecycleException {
        if (treatment.getStatus().ordinal() != Treatment.Status.WAITING.ordinal())
            throw new LifecycleException();
    }

    @Override
    public void assertReschedule(Treatment treatment) throws LifecycleException {
        if (treatment.getStatus().ordinal() != Treatment.Status.WAITING.ordinal())
            throw new LifecycleException();
    }
}
