package ch.obya.education.android.impatient.api.controller;

import static ch.obya.education.android.impatient.api.TreatmentApi.ALL_TREATMENTS_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.CANCEL_CURRENT_TREATMENT_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.CURRENT_USER_TREATMENT_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.DELAY_CURRENT_TREATMENT_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.DELAY_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.MINUTES_COUNT;
import static ch.obya.education.android.impatient.api.TreatmentApi.ON_TREATMENT_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.RESCHEDULE_MOVE;
import static ch.obya.education.android.impatient.api.TreatmentApi.RESCHEDULE_MOVE_X;
import static ch.obya.education.android.impatient.api.TreatmentApi.RESCHEDULE_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.TREATMENT_ENDPOINT_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.TREATMENT_ID;
import static ch.obya.education.android.impatient.api.TreatmentApi.VERIFY_CURRENT_TREATMENT_PATH;
import static ch.obya.education.android.impatient.api.TreatmentApi.VERIFY_PATH;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ch.obya.education.android.impatient.api.TreatmentApi.Reschedule;
import ch.obya.education.android.impatient.domain.logic.TreatmentRoom;
import ch.obya.education.android.impatient.domain.logic.TreatmentSet;
import ch.obya.education.android.impatient.domain.logic.WaitingRoom;
import ch.obya.education.android.impatient.domain.model.Treatment;
import ch.obya.education.android.impatient.repository.ReservationRepository;
import ch.obya.education.android.impatient.security.Authorities;

/**
 * Created by olivier on 24.10.15.
 */
@Controller
@RequestMapping(value = TREATMENT_ENDPOINT_PATH)
public class TreatmentController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(TreatmentController.class);

    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private WaitingRoom waitingRoom;
    @Autowired
    private TreatmentRoom treatmentRoom;
    @Autowired
    private TreatmentSet treatments;

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = ALL_TREATMENTS_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Treatment> getAll() {
        return treatments.elements();
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = DELAY_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    void delay(@PathVariable(TREATMENT_ID) String treatmentId, @RequestParam(MINUTES_COUNT) int minutesCount) {
        waitingRoom.delay(getTreatmentById(treatmentId), minutesCount);
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = VERIFY_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    void verify(@PathVariable(TREATMENT_ID) String treatmentId) {
        treatmentRoom.verify(getTreatmentById(treatmentId));
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = RESCHEDULE_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    void reschedule(@PathVariable(TREATMENT_ID) String treatmentId,
                 @RequestParam(RESCHEDULE_MOVE) Reschedule reschedule,
                 @RequestParam(RESCHEDULE_MOVE_X) int stepCount) {
        waitingRoom.reschedule(getTreatmentById(treatmentId), WaitingRoom.Reschedule.valueOf(reschedule.name()), stepCount);
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = ON_TREATMENT_PATH, method = RequestMethod.DELETE)
    public
    @ResponseBody
    void cancel(@PathVariable(TREATMENT_ID) String treatmentId) {
        waitingRoom.cancel(getTreatmentById(treatmentId));
    }

    @Secured(Authorities.ROLE_PATIENT)
    @RequestMapping(value = CANCEL_CURRENT_TREATMENT_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    void cancel(Principal principal) {
        waitingRoom.cancel(getTreatmentByPatientId(principal.getName()));
    }

    @Secured(Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_TREATMENT_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    Treatment current(Principal principal) {
        return getTreatmentByPatientId(principal.getName());
    }

    @Secured(Authorities.ROLE_PATIENT)
    @RequestMapping(value = DELAY_CURRENT_TREATMENT_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    void delay(@RequestParam(MINUTES_COUNT) int minutesCount, Principal principal) {
        waitingRoom.delay(getTreatmentByPatientId(principal.getName()), minutesCount);
    }

    @Secured(Authorities.ROLE_PATIENT)
    @RequestMapping(value = VERIFY_CURRENT_TREATMENT_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    void verify(Principal principal) {
        treatmentRoom.verify(getTreatmentByPatientId(principal.getName()));
    }


    private Treatment getTreatmentByPatientId(String patientId) {
        Treatment treatment;
        if ((treatment = treatments.getByPatient(patientId)) != null) {
            return treatment;
        }
        else throw new ResourceNotFoundException(patientId);
    }

    private Treatment getTreatmentById(String treatmentId) {
        Treatment treatment;
        if ((treatment = treatments.get(treatmentId)) != null) {
            return treatment;
        }
        else throw new ResourceNotFoundException(treatmentId);
    }
}
