package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Treatment;

/**
 * Created by olivier on 26.10.15.
 */
public interface TreatmentRoom {

    String TREATMENT_ROOM_TOPIC = "treatmentRoom";

    Treatment verify(final Treatment treatment);
}
