package ch.obya.education.android.impatient.api.controller;

import static ch.obya.education.android.impatient.api.PatientApi.CURRENT_USER_PATIENT_PATH;
import static ch.obya.education.android.impatient.api.PatientApi.CURRENT_USER_PHOTO_PATH;
import static ch.obya.education.android.impatient.api.PatientApi.DATA;
import static ch.obya.education.android.impatient.api.PatientApi.ON_PATIENT_PATH;
import static ch.obya.education.android.impatient.api.PatientApi.ON_PERSONS_PATH;
import static ch.obya.education.android.impatient.api.PatientApi.ON_PERSON_PATH;
import static ch.obya.education.android.impatient.api.PatientApi.ON_PHOTO_PATH;
import static ch.obya.education.android.impatient.api.PatientApi.PATIENT_ENDPOINT_PATH;
import static ch.obya.education.android.impatient.api.PatientApi.PATIENT_ID;
import static ch.obya.education.android.impatient.api.PatientApi.PATIENT_IDS;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import ch.obya.education.android.impatient.domain.model.MedicalRecord;
import ch.obya.education.android.impatient.domain.model.Person;
import ch.obya.education.android.impatient.repository.PatientFileManager;
import ch.obya.education.android.impatient.repository.PatientRepository;
import ch.obya.education.android.impatient.security.Authorities;


/**
 * Created by olivier on 24.10.15.
 */
@Controller
@RequestMapping(value = PATIENT_ENDPOINT_PATH)
public class PatientController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(PatientController.class);

    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private PatientFileManager patientFileManager;

    @Secured (Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_PATIENT_PATH, method = RequestMethod.GET)
    public MedicalRecord getCurrentUserMedicalRecord(Principal principal) throws IOException {
        return loadMedicalRecord(principal.getName());
    }

    @Secured (Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_PATIENT_PATH, method = RequestMethod.PUT)
    public
    @ResponseBody
    MedicalRecord update(@RequestBody MedicalRecord record, Principal principal) {
        record.setId(principal.getName());
        return patientRepository.save(record);
    }

    @Secured (Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_PATIENT_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    MedicalRecord create(@RequestBody MedicalRecord record, Principal principal) {
        record.setId(principal.getName());
        return patientRepository.save(record);
    }

    @Secured (Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_PHOTO_PATH, method = RequestMethod.GET)
    public void getCurrentPhotoData(HttpServletResponse response,
                                    HttpServletRequest request,
                                    Principal principal) throws IOException {
        MedicalRecord record = loadMedicalRecord(principal.getName());
        if (patientFileManager.hasPhotoData(record)) {
            response.setHeader("Content-Disposition", "attachment; filename=" + record.getId());
//            response.setContentType(record.getContentType());
            OutputStream outputStream = response.getOutputStream();
            patientFileManager.copyPhotoData(record, outputStream);
        }
        else throw new ResourceNotFoundException(principal.getName());
    }

    @Secured (Authorities.ROLE_PATIENT)
    @RequestMapping(value = CURRENT_USER_PHOTO_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    void setPhotoData(@RequestParam(DATA) List<MultipartFile> data, Principal principal) throws IOException {
        MedicalRecord record = loadMedicalRecord(principal.getName());
        InputStream inputStream = data.get(0).getInputStream();
        patientFileManager.savePhotoData(record, inputStream);
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = ON_PERSONS_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    List<Person> getPersons(@RequestParam(value = PATIENT_IDS) List<String> patientIds) {
        return patientRepository.findAllPersonById(patientIds);
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = ON_PERSON_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    Person getPerson(@PathVariable(value = PATIENT_ID) String patientId) {
        return loadPerson(patientId);
    }

    @Secured(Authorities.ROLE_ADMIN)
    @RequestMapping(value = ON_PATIENT_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    MedicalRecord getMedicalRecord(@RequestParam(value = PATIENT_ID) String patientId) {
        return loadMedicalRecord(patientId);
    }

    @Secured (Authorities.ROLE_ADMIN)
    @RequestMapping(value= ON_PHOTO_PATH, method=RequestMethod.GET)
    public void getPhotoData(@PathVariable(PATIENT_ID) String patientId,
                             HttpServletResponse response) throws IOException {
        MedicalRecord record = loadMedicalRecord(patientId);
        if (patientFileManager.hasPhotoData(record)) {
            response.setHeader("Content-Disposition", "attachment; filename=" + record.getId());
//            response.setContentType(record.getContentType());
            OutputStream outputStream = response.getOutputStream();
            patientFileManager.copyPhotoData(record, outputStream);
        }
        else throw new ResourceNotFoundException(patientId);
    }

    @Secured (Authorities.ROLE_ADMIN)
    @RequestMapping(value = ON_PATIENT_PATH, method = RequestMethod.DELETE)
    public
    @ResponseBody
    void delete(@PathVariable(PATIENT_ID) String id) {
        //TODO
    }

    private MedicalRecord loadMedicalRecord(String patientId) throws ResourceNotFoundException {
        MedicalRecord record = patientRepository.findOne(patientId);
        if (record != null) {
            return record;
        }
        else throw new ResourceNotFoundException(patientId);
    }

    private Person loadPerson(String patientId) throws AbstractController.ResourceNotFoundException {
        Person person = patientRepository.findPersonById(patientId);
        if (person != null) {
            return person;
        }
        else throw new ResourceNotFoundException(patientId);
    }
}
