package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Treatment;

import java.util.List;

/**
 * Created by olivier on 15.11.15.
 */
public interface TreatmentTimeEstimator {
    Treatment adjustEstimate(Treatment treatment);

    List<Treatment> adjustEstimates();
}
