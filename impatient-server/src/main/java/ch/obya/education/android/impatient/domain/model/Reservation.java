package ch.obya.education.android.impatient.domain.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by olivier on 26.10.15.
 */
@Entity
public class Reservation {
    @Id
    private String id;
    @JsonIgnore
    private Date day;
    @JsonIgnore
    private Byte slotId;
    private Date scheduleTime;
    private String patientId;
    private Date checkInTime;
    private String treatmentId;

    public Reservation(Date scheduleTime, Date day, Byte slotId, String patientId) {
        this.id = UUID.randomUUID().toString();
        this.patientId = patientId;
		this.day = day;
		this.slotId = slotId;
		this.scheduleTime = scheduleTime;
    }

    public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Byte getSlotId() {
		return slotId;
	}

	public void setSlotId(Byte slotId) {
		this.slotId = slotId;
	}

	public Date getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(Date scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPatientId() {
        return patientId;
    }

    public String getId() {
        return id;
    }

    public Date getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(Date checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(String treatmentId) {
        this.treatmentId = treatmentId;
    }
}
