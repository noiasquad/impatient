package ch.obya.education.android.impatient.domain.logic;

import ch.obya.education.android.impatient.domain.model.Reservation;
import ch.obya.education.android.impatient.domain.model.Treatment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by olivier on 26.10.15.
 */
@Component
public class WaitingRoomImpl implements WaitingRoom, MessageBroker.Consumer {

    @Autowired
    private TreatmentSet treatments;
    @Autowired
    private TreatmentLifecycle treatmentLifecycle;
    @Autowired
    private TreatmentTimeEstimator treatmentTimeEstimator;
    @Autowired
    private MessageBroker messageBroker;

    @Override
    public Treatment checkIn(Reservation reservation) {
        Treatment treatment = treatmentLifecycle.check(new Treatment(reservation.getPatientId()));
        return updateEstimatedScheduleTime(treatment);
    }

    @Override
    public Treatment delay(Treatment treatment, int minutes) {
        treatmentLifecycle.assertDelay(treatment);
        treatment = treatments.shift(treatment.getId(), -1);
        updateEstimatedScheduleTimes();
        return treatment;
    }

    @Override
    public Treatment reschedule(Treatment treatment, Reschedule reschedule, int rankCount) {
        treatmentLifecycle.assertReschedule(treatment);
        switch (reschedule) {
            case TO_FIRST:
                treatment = treatments.toFirst(treatment.getId());
                updateEstimatedScheduleTimes();
                break;
            case TO_LAST:
                treatment = treatments.toLast(treatment.getId());
                updateEstimatedScheduleTimes();
                break;
            case SHIFT:
                treatment = treatments.shift(treatment.getId(), rankCount);
                updateEstimatedScheduleTimes();
                break;
        }
        return treatment;
    }

    @Override
    public void cancel(Treatment treatment) {
        treatmentLifecycle.cancel(treatment);
    }

    @Async
    private Treatment updateEstimatedScheduleTime(Treatment treatment) {
        treatment = treatmentTimeEstimator.adjustEstimate(treatment);
        messageBroker.publish(treatment.getPatientId(), createScheduleTimeMessage(treatment));
        return treatment;
    }

    @Async
    private void updateEstimatedScheduleTimes() {
        for (Treatment treatment : treatmentTimeEstimator.adjustEstimates()) {
            messageBroker.publish(treatment.getPatientId(), createScheduleTimeMessage(treatment));
        }
    }

    private static final DateFormat timeFormat = new SimpleDateFormat("HH:mm");

    private Map<String, String> createScheduleTimeMessage(Treatment treatment) {
        Map<String,String> message = new HashMap<String, String>();
        message.put("ESTIMATED_TREATMENT_TIME", timeFormat.format(treatment.getScheduleTime()));
        message.put("PATIENT_ID", treatment.getPatientId());
        return message;
    }

    @Override
    public void consume(List<Map<String, String>> messages) {
        for (Map<String,String> message : messages) {
            if (message.containsKey("TREATMENT_CLOSE_TO_END")) {
                promoteNext();
            }
        }
    }

    private void promoteNext() {
        Treatment candidate = treatments.promoteNext();
        treatmentLifecycle.promote(candidate);
    }
}

