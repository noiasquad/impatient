package ch.obya.education.android.impatient.repository;

import ch.obya.education.android.impatient.domain.model.MedicalRecord;
import ch.obya.education.android.impatient.domain.model.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by olivier on 26.10.15.
 */
public interface PatientRepository extends CrudRepository<MedicalRecord, String> {

    Person findPersonById(String id);

    List<Person> findAllPersonById(List<String> ids);
}
