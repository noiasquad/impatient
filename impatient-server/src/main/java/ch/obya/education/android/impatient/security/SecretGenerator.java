package ch.obya.education.android.impatient.security;

import java.util.Calendar;

/**
 * Created by olivier on 15.11.15.
 */
public class SecretGenerator {
    public static String generate() {
        Long randomNumber = Calendar.getInstance().getTimeInMillis() % 10000;
        return randomNumber.toString();
    }
}
