package ch.obya.education.android.impatient.domain.logic;

/**
 * Created by olivier on 11.11.15.
 */
public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
