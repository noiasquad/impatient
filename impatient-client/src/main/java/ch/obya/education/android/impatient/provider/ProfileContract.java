package ch.obya.education.android.impatient.provider;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * This contract defines the metadata for the ProfileContentProvider, including
 * the provider's access URIs and its "database" constants.
 */
public class ProfileContract {
	/**
	 * This ContentProvider's unique identifier.
	 */
	public static final String CONTENT_AUTHORITY = "ch.obya.education.android.impatient.provider";

	/**
	 * Use CONTENT_AUTHORITY to create the base of all URI's which apps will use
	 * to contact the content provider.
	 */
	public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

	/**
	 * Possible paths (appended to base content URI for possible URI's).
	 */
	public static final String PATH_PROFILE = ProfileEntry.TABLE_NAME;

	/*
	 * Columns
	 */

	/**
	 * Inner class that defines the table contents of the Profile table.
	 */
	public static final class ProfileEntry implements BaseColumns {
		/**
		 * Use BASE_CONTENT_URI to create the unique URI for Acronym Table that
		 * apps will use to contact the content provider.
		 */
		public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_PROFILE).build();

		/**
		 * When the Cursor returned for a given URI by the ContentProvider
		 * contains 0..x items.
		 */
		public static final String CONTENT_ITEMS_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_PROFILE;

		/**
		 * When the Cursor returned for a given URI by the ContentProvider
		 * contains 1 item.
		 */
		public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_PROFILE;

		/**
		 * Columns to display.
		 */
		public static final String sColumnsToDisplay[] = new String[] {
			ProfileContract.ProfileEntry._ID,
			ProfileContract.ProfileEntry.COLUMN_FIRST_NAME,
			ProfileContract.ProfileEntry.COLUMN_LAST_NAME,
			ProfileContract.ProfileEntry.COLUMN_PHOTO_URL };

		/**
		 * Name of the database table.
		 */
		public static final String TABLE_NAME = "profile_table";

		/**
		 * Columns to store data.
		 */
		public static final String COLUMN_FIRST_NAME = "firstname";
		public static final String COLUMN_LAST_NAME = "lastname";
		public static final String COLUMN_PHOTO_URL = "photoUrl";
		public static final String BIRTH_DATE = "birthdate";

		/**
		 * Return a Uri that points to the row containing a given id.
		 * 
		 * @param id
		 * @return Uri
		 */
		public static Uri buildUri(Long id) {
			return ContentUris.withAppendedId(CONTENT_URI, id);
		}
	}
}
