package ch.obya.education.android.impatient.service;

import android.app.IntentService;
import android.content.Intent;


/**
 * For patient role only
 * 
 * A service that concurrently polls the last messages sent by the server
 * in case of the one way SSE communication between server and client has not been established.
 * This service consumes the messages stored on the server at the topic assigned to the patient.
 * A repeated alarm (every 5 minutes) and an intent starts the service.
 */
public class MessagePollingService extends IntentService {

	public MessagePollingService(String patientId) {
		super(patientId);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
	}
}
