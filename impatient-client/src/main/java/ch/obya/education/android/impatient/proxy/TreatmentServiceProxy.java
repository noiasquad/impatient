package ch.obya.education.android.impatient.proxy;

import java.util.List;

import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import ch.obya.education.android.impatient.data.model.Treatment;
import ch.obya.education.android.impatient.proxy.api.TreatmentApi;

/**
 * Created by olivier on 05.11.15.
 */
public interface TreatmentServiceProxy extends TreatmentApi {
    
    @GET(TREATMENT_ENDPOINT_PATH + ALL_TREATMENTS_PATH)
	List<Treatment> allTreatments();

    @POST(TREATMENT_ENDPOINT_PATH + DELAY_PATH)
    void delay(@Path(TREATMENT_ID) String treatmentId, @Query(MINUTES_COUNT) int minutesCount);

	@POST(TREATMENT_ENDPOINT_PATH + VERIFY_PATH)
    void verify(@Path(TREATMENT_ID) String treatmentId);

	@POST(TREATMENT_ENDPOINT_PATH + RESCHEDULE_PATH)
    void reschedule(@Path(TREATMENT_ID) String treatmentId,
    				@Query(RESCHEDULE_MOVE) Reschedule reschedule,
					@Query(RESCHEDULE_MOVE_X) int rankCount);

    @DELETE(TREATMENT_ENDPOINT_PATH + ON_TREATMENT_PATH)
    void cancel(@Path(TREATMENT_ID) String treatmentId);

    @POST(TREATMENT_ENDPOINT_PATH + CANCEL_CURRENT_TREATMENT_PATH)
    void cancel();

    @GET(TREATMENT_ENDPOINT_PATH + CURRENT_USER_TREATMENT_PATH)
    Treatment current();

    @POST(TREATMENT_ENDPOINT_PATH + DELAY_CURRENT_TREATMENT_PATH)
    void delay(@Query(MINUTES_COUNT) int minutesCount);

    @POST(TREATMENT_ENDPOINT_PATH + VERIFY_CURRENT_TREATMENT_PATH)
    void verify();
}
