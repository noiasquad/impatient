package ch.obya.education.android.impatient.activity.ui;

import java.text.SimpleDateFormat;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import ch.obya.education.android.impatient.activity.ProfileActivity;
import ch.obya.education.android.impatient.data.model.Treatment;
import ch.obya.education.coursera.impatient.R;

public class TreatmentListAdapter extends ArrayAdapter<Treatment> {
	
	public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
	private final static int IMAGE_VIEW_WIDTH = 80;
	private final static int IMAGE_VIEW_HEIGHT = 100;

	public TreatmentListAdapter(Context context) {
		super(context, R.layout.layout_treatment_item);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final Treatment item = getItem(position);
		
		FrameLayout itemLayout = convertView == null ?
				(FrameLayout)LayoutInflater.from(getContext()).inflate(R.layout.layout_treatment_item, null) :
					(FrameLayout)convertView;

		final TextView scheduleTime = (TextView) itemLayout.findViewById(R.id.schedule_time);
		scheduleTime.setText(DATE_FORMAT.format(item.getScheduleTime()));

        final TextView patientName = (TextView) itemLayout.findViewById(R.id.patient_name);
        patientName.setText(item.getPatientName());
		
		final ImageView patientPhoto = (ImageView) itemLayout.findViewById(R.id.patient_image_view);
		
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showPatientProfile(item.getPatientId());
			}
		});

		return itemLayout;
	}

	private void showPatientProfile(String patientId) {
		getContext().startActivity(ProfileActivity.displayProfileIntent(getContext(), patientId));
	}
}
