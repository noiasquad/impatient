package ch.obya.education.android.impatient.proxy.api;

/**
 * @author olivier
 */
public interface UserApi {

    String USER_ID = "id";
    String USER_ENDPOINT_PATH = "/api/user/v1";
    String ALL_USERS_PATH = "/users";
    String CURRENT_USER_PATH = ALL_USERS_PATH + "/current";
    String CURRENT_USER_ID_PATH = CURRENT_USER_PATH + "/id";
    String CURRENT_USER_SECRET_PATH = CURRENT_USER_PATH + "/secret";
}
