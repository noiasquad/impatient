package ch.obya.education.android.impatient.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import ch.obya.education.android.common.GenericActivity;
import ch.obya.education.android.impatient.operation.LoginOps;
import ch.obya.education.coursera.impatient.R;

/**
 * The activity that allows the user to provide login information.
 */
public class LoginActivity extends GenericActivity<LoginOps> {

	static final String LOG_TAG = LoginActivity.class.getCanonicalName();

	EditText userId;
	EditText userSecret;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_login);
		userId = (EditText) findViewById(R.id.username);
		userSecret = (EditText) findViewById(R.id.password);

		// Call up to the special onCreate() method in
		// GenericActivity, passing in the LoginOps class to
		// instantiate and manage.
		super.onCreate(savedInstanceState, LoginOps.class);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		userSecret.setText("");
	}

	/**
	 * The sign in button has been clicked.
	 */
	public void signInClicked(View v) {
		getOps().authenticate(userId.getText().toString(), userSecret.getText().toString());
		userSecret.setText("");
	}

    /**
     * The sign up button has been clicked.
     */
    public void signUpClicked(View v) {
    }
}
