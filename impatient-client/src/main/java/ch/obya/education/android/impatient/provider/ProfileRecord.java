package ch.obya.education.android.impatient.provider;

/**
 * A simple POJO that stores information about Hobbit characters.
 */
class ProfileRecord {
    /**
     * Start at 0.
     */
    static long sInitialId = 0;

    /**
     * Id of the character in the map.
     */
    private final long mId;

    /**
     * Name of the character.
     */
    private final String mName;

    /**
     * Race of the character.
     */
    private String mRace;

    /**
     * Constructor initializes all the name and race fields.
     */
    ProfileRecord(String name,
                    String race) {
        mName = name;
        mRace = race;
        mId = ++sInitialId;
    }

    /**
     * Constructor initializes all the fields.
     */
    ProfileRecord(long id,
                    String name,
                    String race) {
        mId = id;
        mName = name;
        mRace = race;
    }

    /**
     * @return the id of the character.
     */
    long getId() {
        return mId;
    }

    /**
     * @return the name of the character.
     */
    String getName() {
        return mName;
    }

    /**
     * @return the race of the character.
     */
    String getRace() {
        return mRace;
    }

    /**
     * Set the race of the character.
     */
    void setRace(String race) {
        mRace = race;
    }
}
