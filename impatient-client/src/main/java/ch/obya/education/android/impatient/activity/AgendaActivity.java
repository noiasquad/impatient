package ch.obya.education.android.impatient.activity;

import java.util.List;

import android.os.Bundle;
import android.widget.ListView;
import ch.obya.education.android.common.GenericActivity;
import ch.obya.education.android.impatient.activity.ui.SlotListAdapter;
import ch.obya.education.android.impatient.data.model.Slot;
import ch.obya.education.android.impatient.operation.AgendaOps;
import ch.obya.education.coursera.impatient.R;

public class AgendaActivity extends GenericActivity<AgendaOps> {

    private SlotListAdapter listAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.layout_bookable_slot_list);
        listView = (ListView) findViewById(R.id.bookable_slot_list_view);
        listView.setAdapter(listAdapter = new SlotListAdapter(getApplicationContext()));
        
        super.onCreate(savedInstanceState, AgendaOps.class);
    }

    public void displayList(List<Slot> items) {
		if (items != null) {
			listAdapter.clear();
			listAdapter.addAll(items);
		}
    }
}
