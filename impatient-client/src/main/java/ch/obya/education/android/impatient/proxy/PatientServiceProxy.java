package ch.obya.education.android.impatient.proxy;

import java.util.List;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Streaming;
import retrofit.mime.TypedFile;
import ch.obya.education.android.impatient.data.model.MedicalRecord;
import ch.obya.education.android.impatient.data.model.Person;
import ch.obya.education.android.impatient.proxy.api.PatientApi;

/**
 * Created by olivier on 05.11.15.
 */
public interface PatientServiceProxy extends PatientApi {

    @GET(PATIENT_ENDPOINT_PATH + CURRENT_USER_PATIENT_PATH)
    MedicalRecord getCurrentUserProfile();

    @POST(PATIENT_ENDPOINT_PATH + CURRENT_USER_PATIENT_PATH)
    void createCurrentUserProfile(@Body MedicalRecord medicalRecord);

    @PUT(PATIENT_ENDPOINT_PATH + CURRENT_USER_PATIENT_PATH)
    void setCurrentUserProfile(@Body MedicalRecord medicalRecord);

    @Streaming
    @GET(PATIENT_ENDPOINT_PATH + CURRENT_USER_PHOTO_PATH)
    Response getCurrentUserPhotoData();

    @Multipart
    @POST(PATIENT_ENDPOINT_PATH + CURRENT_USER_PHOTO_PATH)
    void setCurrentUserPhotoData(@Part(DATA) TypedFile data);

    @GET(PATIENT_ENDPOINT_PATH + ON_PERSON_PATH)
    Person getPerson(@Path(PATIENT_ID) String patientId);

    @GET(PATIENT_ENDPOINT_PATH + ON_PERSONS_PATH)
    List<Person> getPersons(@Path(PATIENT_IDS) List<String> patientIds);

    @GET(PATIENT_ENDPOINT_PATH + ON_PATIENT_PATH)
    MedicalRecord getMedicalRecord(@Path(PATIENT_ID) String patientId);

    @POST(PATIENT_ENDPOINT_PATH + ON_PATIENT_PATH)
    MedicalRecord create(@Body MedicalRecord record);

    @Multipart
    @POST(PATIENT_ENDPOINT_PATH + ON_PHOTO_PATH)
    void setPhotoData(@Path(PATIENT_ID) String patientId, @Part(DATA) TypedFile data);

    @Streaming
    @GET(PATIENT_ENDPOINT_PATH + ON_PHOTO_PATH)
    Response getPhotoData(@Path(PATIENT_ID) String patientId);

    @PUT(PATIENT_ENDPOINT_PATH + ON_PATIENT_PATH)
    MedicalRecord update(@Path(PATIENT_ID) String id, @Body MedicalRecord record);

    @DELETE(PATIENT_ENDPOINT_PATH + ON_PATIENT_PATH)
    void delete(@Path(PATIENT_ID) String id);
}
