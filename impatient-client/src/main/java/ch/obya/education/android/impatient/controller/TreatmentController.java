package ch.obya.education.android.impatient.controller;

import android.content.Context;
import ch.obya.education.android.impatient.proxy.TreatmentServiceProxy;

/**
 * Created by olivier on 05.11.15.
 */
public class TreatmentController {
    /**
     * Allows access to application-specific resources and classes.
     */
    private Context context;

    /**
     * Defines methods that communicates with Video Service.
     */
    private TreatmentServiceProxy treatmentApi;

    /**
     * Constructor that initializes the VideoController.
     *
     * @param context
     */
    public TreatmentController(Context context) {
    }

}
