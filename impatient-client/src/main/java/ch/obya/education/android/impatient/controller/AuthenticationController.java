package ch.obya.education.android.impatient.controller;

import ch.obya.education.android.impatient.proxy.ApiProxyFactory;
import ch.obya.education.android.impatient.proxy.ApiProxy;
import ch.obya.education.android.impatient.proxy.ApiProxyInstance;

public class AuthenticationController {
	public static boolean authenticate(String username, String password, String serverUrl) {
		try {
            ApiProxyInstance.setInstance(ApiProxyFactory.create(username, password, serverUrl, ApiProxy.class));
			return username.equals(ApiProxyInstance.getInstance().getCurrentUser());
		}
		catch (Exception e) {
			return false;
		}
	}

    public static boolean changeSecret(String oldSecret, String newSecret) {
        try {
            ApiProxyInstance.getInstance().changeSecret(oldSecret, newSecret);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public static String[] createUser() {
        try {
            return ApiProxyInstance.getInstance().createUser();
        }
        catch (Exception e) {
            return null;
        }
    }
}
