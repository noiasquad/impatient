package ch.obya.education.android.impatient.proxy.api;

/**
 * @author olivier
 */
public interface ReservationApi {

    String RESERVATION_ID = "id";
    String DAY_ID = "date";
    String SLOT_ID = "time";

    String PREFERRED_DAYS = "preferred-days";
    String PREFERRED_TIMES = "preferred-times";

    String RESERVATION_ENDPOINT_PATH = "api/reservation/v1";
    String CURRENT_USER_RESERVATIONS_PATH = "/users/current/reservations";
    String CURRENT_USER_PREFERRED_SLOTS_PATH = "/users/current/slots/preferred/free";
    String FREE_SLOTS_PATH = "/slots/free";
    String CURRENT_USER_RESERVE_PATH = "/days/{" + DAY_ID + "}/slots/{" + SLOT_ID + "}";
    String ON_RESERVATION_PATH = "/{" + RESERVATION_ID + "}";
    String CHECK_IN_PATH = "/{" + RESERVATION_ID + "}/check-in";
    String SLOTS_IN_DAY_PATH = "/slots/days/{" + DAY_ID + "}";
    String SLOTS_IN_WEEK_PATH = "/slots/weeks/{" + DAY_ID + "}";
    String SLOTS_IN_MONTH_PATH = "/slots/months/{" + DAY_ID + "}";
    String TODAY_SLOTS_PATH = "/slots/days/current";
    String THIS_WEEK_SLOTS_PATH = "/slots/weeks/current";
    String THIS_MONTH_SLOTS_PATH = "/slots/months/current";
}
