package ch.obya.education.android.impatient.proxy;


/**
 * Created by olivier on 08.11.15.
 */
public interface AuthServiceProxy {

    String TOKEN_PATH = "/oauth/token";
}
