package ch.obya.education.android.impatient.provider;

import java.io.File;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * The database helper used by the profile Content Provider to create and manage
 * its underling database.
 */
public class ProfileDatabaseHelper extends SQLiteOpenHelper {
	/**
	 * Database name.
	 */
	private static String DATABASE_NAME = "ch_obya_education_coursera_impatient_db";

	/**
	 * Database version number, which is updated with each schema change.
	 */
	private static int DATABASE_VERSION = 1;

	/*
	 * SQL create table statements.
	 */

	/**
	 * SQL statement used to create the Profile table.
	 */
	final String SQL_CREATE_PROFILE_TABLE =
			"CREATE TABLE " + ProfileContract.ProfileEntry.TABLE_NAME +
			" (" + ProfileContract.ProfileEntry._ID + " INTEGER PRIMARY KEY, " +
					ProfileContract.ProfileEntry.COLUMN_FIRST_NAME + " TEXT NOT NULL, " +
					ProfileContract.ProfileEntry.COLUMN_LAST_NAME + " TEXT NOT NULL " +
					ProfileContract.ProfileEntry.BIRTH_DATE + " DATE NOT NULL " +
					ProfileContract.ProfileEntry.COLUMN_PHOTO_URL + " TEXT NOT NULL " + " );";

	/**
	 * Constructor - initialize database name and version, but don't actually
	 * construct the database (which is done in the onCreate() hook method). It
	 * places the database in the application's cache directory, which will be
	 * automatically cleaned up by Android if the device runs low on storage
	 * space.
	 * 
	 * @param context
	 */
	public ProfileDatabaseHelper(Context context) {
		super(context, context.getCacheDir() + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * Hook method called when the database is created.
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Create the table.
		db.execSQL(SQL_CREATE_PROFILE_TABLE);
	}

	/**
	 * Hook method called when the database is upgraded.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Delete the existing tables.
		db.execSQL("DROP TABLE IF EXISTS " + ProfileContract.ProfileEntry.TABLE_NAME);
		// Create the new tables.
		onCreate(db);
	}
}
