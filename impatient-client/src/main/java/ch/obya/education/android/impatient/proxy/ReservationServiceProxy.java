package ch.obya.education.android.impatient.proxy;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import ch.obya.education.android.impatient.data.model.BookableSlot;
import ch.obya.education.android.impatient.data.model.Reservation;
import ch.obya.education.android.impatient.data.model.Slot;
import ch.obya.education.android.impatient.proxy.api.ReservationApi;

/**
 * Created by olivier on 05.11.15.
 */
public interface ReservationServiceProxy extends ReservationApi{

    @GET(RESERVATION_ENDPOINT_PATH + CURRENT_USER_RESERVATIONS_PATH)
    public List<Reservation> getCurrentUserReservations(Principal principal);

    @GET(RESERVATION_ENDPOINT_PATH + FREE_SLOTS_PATH)
    public List<BookableSlot> getFreeSlots();

    @POST(RESERVATION_ENDPOINT_PATH + CURRENT_USER_RESERVE_PATH)
    public Reservation createReservation(@Path(DAY_ID) Date day, @Path(SLOT_ID) Byte slotId);

    @GET(RESERVATION_ENDPOINT_PATH + TODAY_SLOTS_PATH)
    public List<Slot> getTodayAgenda();

    @GET(RESERVATION_ENDPOINT_PATH + SLOTS_IN_DAY_PATH)
    public List<Slot> getDayAgenda(@Path(DAY_ID) Date day);

    @GET(RESERVATION_ENDPOINT_PATH + THIS_WEEK_SLOTS_PATH)
    public List<Slot> getWeekAgenda();

    @GET(RESERVATION_ENDPOINT_PATH + SLOTS_IN_WEEK_PATH)
    public List<Slot> getWeekAgenda(@Path(DAY_ID) Date dateInWeek);

    @GET(RESERVATION_ENDPOINT_PATH + THIS_MONTH_SLOTS_PATH)
    public List<Slot> getMonthAgenda();

    @GET(RESERVATION_ENDPOINT_PATH + SLOTS_IN_MONTH_PATH)
    public List<Slot> getMonthAgenda(@Path(DAY_ID) Date dateInMonth);

    @POST(RESERVATION_ENDPOINT_PATH + CHECK_IN_PATH)
    public String checkIn(@Path(RESERVATION_ID) String reservationId);

    @DELETE(RESERVATION_ENDPOINT_PATH + ON_RESERVATION_PATH)
    public void cancel(@Path(RESERVATION_ID) String reservationId);
}
