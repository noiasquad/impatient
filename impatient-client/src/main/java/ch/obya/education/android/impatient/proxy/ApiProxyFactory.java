package ch.obya.education.android.impatient.proxy;

import ch.obya.education.android.impatient.security.ClientCredentials;
import ch.obya.education.android.impatient.security.SecuredRestBuilder;
import ch.obya.education.android.impatient.security.UnsafeHttpsClient;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;

public class ApiProxyFactory {

    private static ApiProxy instance = null;

	private ApiProxyFactory() {}

	public static <E> E create(String username, String password, String serverUrl, Class<E> clazz) {
        return new SecuredRestBuilder()
		.setLoginEndpoint(serverUrl + AuthServiceProxy.TOKEN_PATH)
		.setUsername(username)
		.setPassword(password)
		.setClientId(ClientCredentials.CLIENT_ID)
		.setClient(new OkClient(UnsafeHttpsClient.getUnsafeOkHttpClient()))
		.setEndpoint(serverUrl).setLogLevel(LogLevel.FULL).build()
		.create(clazz);
	}
}
