package ch.obya.education.android.impatient.operation;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.lang.ref.WeakReference;

import ch.obya.education.android.common.ConfigurableOps;
import ch.obya.education.android.common.GenericAsyncTask;
import ch.obya.education.android.common.GenericAsyncTaskOps;
import ch.obya.education.android.impatient.activity.ProfileActivity;
import ch.obya.education.android.impatient.data.model.MedicalRecord;
import ch.obya.education.android.impatient.proxy.ApiProxyInstance;

public class ProfileOps implements ConfigurableOps {

    /**
     * Debugging tag used by the Android logger.
     */
    private static final String TAG = ProfileOps.class.getSimpleName();

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<ProfileActivity> mActivity;

    /**
     * It allows access to application-specific resources.
     */
    private Context mApplicationContext;

    /**
     * Default constructor that's needed by the GenericActivity framework.
     */
    public ProfileOps() {
    }

    /**
     * Called after a runtime configuration change occurs to finish the initialisation steps.
     */
    public void onConfiguration(Activity activity, boolean firstTimeIn) {
        final String time = firstTimeIn ? "first time" : "second+ time";

        Log.d(TAG, "onConfiguration() called the " + time + " with activity = " + activity);

        mActivity = new WeakReference<>((ProfileActivity) activity);

        if (firstTimeIn) {
            mApplicationContext = activity.getApplicationContext();
            getProfileData();
        }
    }

    public void setProfileData(MedicalRecord medicalRecord) {
        new GenericAsyncTask<>(new GenericAsyncTaskOps<MedicalRecord, Void, Boolean>() {
            @Override
            public Boolean doInBackground(MedicalRecord... params) {
                ApiProxyInstance.getInstance().setCurrentUserProfile(params[0]);
                return true;
            }

            @Override
            public void onPostExecute(Boolean result) {
            }
        }).execute(medicalRecord);
    }

    public void getProfileData() {
        new GenericAsyncTask<>(new GenericAsyncTaskOps<Void, Void, MedicalRecord>() {
            @Override
            public MedicalRecord doInBackground(Void... params) {
                return ApiProxyInstance.getInstance().getCurrentUserProfile();
            }

            @Override
            public void onPostExecute(MedicalRecord record) {
            }
        }).execute();
    }
}
