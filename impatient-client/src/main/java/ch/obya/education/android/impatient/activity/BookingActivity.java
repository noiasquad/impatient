package ch.obya.education.android.impatient.activity;

import java.util.List;

import android.os.Bundle;
import android.widget.ListView;
import ch.obya.education.android.common.GenericActivity;
import ch.obya.education.android.common.Utils;
import ch.obya.education.android.impatient.activity.ui.BookableSlotListAdapter;
import ch.obya.education.android.impatient.data.model.BookableSlot;
import ch.obya.education.android.impatient.operation.BookingOps;
import ch.obya.education.coursera.impatient.R;

public class BookingActivity extends GenericActivity<BookingOps> {

    private BookableSlotListAdapter listAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.layout_bookable_slot_list);
        listView = (ListView) findViewById(R.id.bookable_slot_list_view);
        listView.setAdapter(listAdapter = new BookableSlotListAdapter(getApplicationContext()));
        
        super.onCreate(savedInstanceState, BookingOps.class);
    }

    public void displayList(List<BookableSlot> items) {
		if (items != null) {
			listAdapter.clear();
			listAdapter.addAll(items);

			if (items.isEmpty()) {
				Utils.showToast(this, "There is no more free slots left.");
			}
		}
    }
}
