package ch.obya.education.android.impatient.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import ch.obya.education.android.common.GenericActivity;
import ch.obya.education.android.impatient.operation.HomeOps;
import ch.obya.education.coursera.impatient.R;

public class HomeActivity extends GenericActivity<HomeOps> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/
    public static Intent openIntent(Activity activity) {
        Intent intent = new Intent();
        intent.setClass(activity, HomeActivity.class);
        return intent;
    }
}
