package ch.obya.education.android.impatient.proxy;

public interface ApiProxy extends
	MessageServiceProxy,
	PatientServiceProxy,
	ReservationServiceProxy,
	TreatmentServiceProxy,
	UserServiceProxy,
	AuthServiceProxy {
}