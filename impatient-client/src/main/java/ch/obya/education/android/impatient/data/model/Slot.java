package ch.obya.education.android.impatient.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Created by olivier on 05.11.15.
 */
public class Slot extends BookableSlot {
	private String reservationId;
    private String patientId;
    @JsonIgnore
    private String patientName;
    @JsonIgnore
    private String photoUrl;
    
	public Slot() {
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getReservationId() {
		return reservationId;
	}

	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}
	
	public String getPatientId() {
		return patientId;
	}
	
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
    
    
}
