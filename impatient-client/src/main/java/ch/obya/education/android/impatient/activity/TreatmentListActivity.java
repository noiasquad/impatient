package ch.obya.education.android.impatient.activity;

import java.util.List;

import android.os.Bundle;
import android.widget.ListView;
import ch.obya.education.android.common.GenericActivity;
import ch.obya.education.android.common.Utils;
import ch.obya.education.android.impatient.activity.ui.TreatmentListAdapter;
import ch.obya.education.android.impatient.data.model.Treatment;
import ch.obya.education.android.impatient.operation.TreatmentOps;
import ch.obya.education.coursera.impatient.R;

public class TreatmentListActivity extends GenericActivity<TreatmentOps> {

    private TreatmentListAdapter listAdapter;
    private ListView treatmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_treatment_list);
        treatmentList = (ListView) findViewById(R.id.treatment_list_view);
        treatmentList.setAdapter(listAdapter = new TreatmentListAdapter(getApplicationContext()));
        
        super.onCreate(savedInstanceState, TreatmentOps.class);
    }

    public void displayList(List<Treatment> treatments) {
		if (treatments != null) {
			listAdapter.clear();
			listAdapter.addAll(treatments);

			if (treatments.isEmpty()) {
				Utils.showToast(this, "No patient in is waiting or is being treated");
			}
		}
    }
}
