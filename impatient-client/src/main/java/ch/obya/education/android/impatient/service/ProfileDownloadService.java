package ch.obya.education.android.impatient.service;

import android.app.IntentService;
import android.content.Intent;


/**
 * For admin role only.
 * 
 * A Bound Service that concurrently downloads the profiles from the server
 * and makes updates into the local database managed by the content provider.
 * This service is launched by an repeated alarm.
 */
public class ProfileDownloadService extends IntentService {

	public ProfileDownloadService(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
	}
}
