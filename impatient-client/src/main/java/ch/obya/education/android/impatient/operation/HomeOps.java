package ch.obya.education.android.impatient.operation;

import java.lang.ref.WeakReference;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import ch.obya.education.android.common.ConfigurableOps;
import ch.obya.education.android.common.GenericAsyncTask;
import ch.obya.education.android.common.GenericAsyncTaskOps;
import ch.obya.education.android.impatient.activity.TreatmentListActivity;
import ch.obya.education.android.impatient.data.model.Treatment;
import ch.obya.education.android.impatient.proxy.ApiProxyInstance;

public class HomeOps implements ConfigurableOps {

	/**
	 * Debugging tag used by the Android logger.
	 */
	private static final String TAG = HomeOps.class.getSimpleName();

	/**
	 * Used to enable garbage collection.
	 */
	private WeakReference<TreatmentListActivity> mActivity;

	/**
	 * It allows access to application-specific resources.
	 */
	private Context mApplicationContext;

	/**
	 * Default constructor that's needed by the GenericActivity framework.
	 */
	public HomeOps() {
	}

	/**
	 * Called after a runtime configuration change occurs to finish the
	 * initialisation steps.
	 */
	public void onConfiguration(Activity activity, boolean firstTimeIn) {
		final String time = firstTimeIn ? "first time" : "second+ time";

		Log.d(TAG, "onConfiguration() called the " + time + " with activity = " + activity);

		mActivity = new WeakReference<>((TreatmentListActivity) activity);

		if (firstTimeIn) {
			mApplicationContext = activity.getApplicationContext();
			getTreatmentsList();
		}
	}
	
    public void getTreatmentsList() {
        new GenericAsyncTask<>(new GenericAsyncTaskOps<Void, Void, List<Treatment>>() {
            @Override
            public List<Treatment> doInBackground(Void... params) {
                return ApiProxyInstance.getInstance().allTreatments();
            }

            @Override
            public void onPostExecute(List<Treatment> treatments) {
            }
        }).execute();
    }
}
