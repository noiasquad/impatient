package ch.obya.education.android.impatient.util;

/**
 * Class that contains all the Constants required in our Video Upload
 * App.
 */
public class Constants {

    //public static final String SERVER_URL = "http://localhost:8080";
    public static final String SERVER_URL = "http://10.0.2.2:8080";

    /**
     * Define a constant for 1 MB.
     */
    public static final long MEGA_BYTE = 1024 * 1024;
    
}
