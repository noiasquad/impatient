package ch.obya.education.android.impatient.operation;

import java.lang.ref.WeakReference;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import ch.obya.education.android.common.ConfigurableOps;
import ch.obya.education.android.common.GenericAsyncTask;
import ch.obya.education.android.common.GenericAsyncTaskOps;
import ch.obya.education.android.common.Utils;
import ch.obya.education.android.impatient.activity.BookingActivity;
import ch.obya.education.android.impatient.activity.HomeActivity;
import ch.obya.education.android.impatient.data.model.BookableSlot;
import ch.obya.education.android.impatient.data.model.Reservation;
import ch.obya.education.android.impatient.proxy.ApiProxyInstance;

public class BookingOps implements ConfigurableOps {

	/**
	 * Debugging tag used by the Android logger.
	 */
	private static final String TAG = BookingOps.class.getSimpleName();

	/**
	 * Used to enable garbage collection.
	 */
	private WeakReference<BookingActivity> activity;

	/**
	 * It allows access to application-specific resources.
	 */
	private Context applicationContext;

	/**
	 * Default constructor that's needed by the GenericActivity framework.
	 */
	public BookingOps() {
	}

	/**
	 * Called after a runtime configuration change occurs to finish the initialization steps.
	 */
	public void onConfiguration(Activity activity, boolean firstTimeIn) {
		final String time = firstTimeIn ? "first time" : "second+ time";

		Log.d(TAG, "onConfiguration() called the " + time + " with activity = " + activity);

		this.activity = new WeakReference<>((BookingActivity) activity);

		if (firstTimeIn) {
			applicationContext = activity.getApplicationContext();
			getFreeSlots();
		}
	}

    public void book(BookableSlot slot) {
    	new GenericAsyncTask<>(new GenericAsyncTaskOps<BookableSlot, Void, Reservation>() {
            @Override
            public Reservation doInBackground(BookableSlot... params) {
                return ApiProxyInstance.getInstance().createReservation(params[0].getDay(), params[0].getSlotId());
            }

            @Override
            public void onPostExecute(Reservation reservation) {
            	if (reservation != null)
					activity.get().startActivity(HomeActivity.openIntent(activity.get()));
				else
					Utils.showToast(activity.get(), "Booking failed or maybe remote service is not available");
            }
        }).execute(slot);
    }

    public void getFreeSlots() {
    	new GenericAsyncTask<>(new GenericAsyncTaskOps<Void, Void, List<BookableSlot>>() {
            @Override
            public List<BookableSlot> doInBackground(Void... params) {
                return ApiProxyInstance.getInstance().getFreeSlots();
            }

            @Override
            public void onPostExecute(List<BookableSlot> result) {
            	activity.get().displayList(result);
            }
        }).execute();
    }
}
