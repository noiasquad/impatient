package ch.obya.education.android.impatient.proxy.api;

/**
 * @author olivier
 */
public interface TreatmentApi {

    String TREATMENT_ID = "id";
    String MINUTES_COUNT = "minutes-count";
    String RESCHEDULE_MOVE = "move";
    String RESCHEDULE_MOVE_X = "count";

    enum Reschedule {
        TO_FIRST, TO_LAST, X_MOVE
    }

    String TREATMENT_ENDPOINT_PATH = "/api/treatment/v1";
    String CURRENT_USER_TREATMENT_PATH = "/users/current/treatments/current";
    String CANCEL_CURRENT_TREATMENT_PATH = CURRENT_USER_TREATMENT_PATH + "/cancel";
    String DELAY_CURRENT_TREATMENT_PATH = CURRENT_USER_TREATMENT_PATH + "/delay";
    String VERIFY_CURRENT_TREATMENT_PATH = CURRENT_USER_TREATMENT_PATH + "/verify";
    String ALL_TREATMENTS_PATH = "/treatments";
    String ON_TREATMENT_PATH = "/treatments/{" + TREATMENT_ID + "}";
    String DELAY_PATH = "/treatments/{" + TREATMENT_ID + "}/delay";
    String VERIFY_PATH = "/treatments/{" + TREATMENT_ID + "}/verify";
    String RESCHEDULE_PATH = "/treatments/{" + TREATMENT_ID + "}/reschedule";
}
