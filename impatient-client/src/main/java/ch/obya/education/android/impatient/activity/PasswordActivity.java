package ch.obya.education.android.impatient.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import ch.obya.education.android.common.GenericActivity;
import ch.obya.education.android.impatient.operation.LoginOps;
import ch.obya.education.coursera.impatient.R;

/**
 * A login screen that offers login via email/password.
 */
public class PasswordActivity extends GenericActivity<LoginOps> {

    static final String LOG_TAG = LoginActivity.class.getCanonicalName();

    EditText userOldSecret;
    EditText userNewSecret;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_password);
        userOldSecret = (EditText) findViewById(R.id.old_password);
        userNewSecret = (EditText) findViewById(R.id.new_password);

        // Call up to the special onCreate() method in
        // GenericActivity, passing in the LoginOps class to
        // instantiate and manage.
        super.onCreate(savedInstanceState, LoginOps.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        userOldSecret.setText("");
        userNewSecret.setText("");
    }

    /**
     * The sign in button has been clicked.
     */
    public void changeClicked(View v) {
        getOps().changeSecret(
                userOldSecret.getText().toString(),
                userNewSecret.getText().toString());

        userOldSecret.setText("");
        userNewSecret.setText("");
    }
}
