package ch.obya.education.android.impatient.proxy.api;

/**
 * @author olivier
 */
public interface MessageApi {

    String MESSAGE_ENDPOINT_PATH = "/api/message/v1";
    String CURRENT_USER_MESSAGES_PATH = "/users/current/messages";
}
