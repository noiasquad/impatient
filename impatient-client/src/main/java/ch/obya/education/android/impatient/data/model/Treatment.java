package ch.obya.education.android.impatient.data.model;

import java.util.Date;

/**
 * Created by olivier on 05.11.15.
 */
public class Treatment {

	public enum Status {
		NONE, WAITING, CANDIDATE, VERIFIED, IN_TREATMENT, TERMINATED
	};

	private String id;
	private String patientId;
	private String patientName;
	private Date scheduleTime;
	private Status status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Date getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(Date scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
