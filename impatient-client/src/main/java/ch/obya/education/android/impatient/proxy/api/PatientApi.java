package ch.obya.education.android.impatient.proxy.api;

/**
 * @author olivier
 */
public interface PatientApi {

    String PATIENT_ID = "id";
    String PATIENT_IDS = "ids";
    String FIELDS = "fields";
    String DATA = "data";

    String PATIENT_ENDPOINT_PATH = "/api/patient/v1";
    String CURRENT_USER_PATIENT_PATH = "/users/current/patient";
    String CURRENT_USER_PHOTO_PATH = CURRENT_USER_PATIENT_PATH + "/photo";
    String ON_PATIENT_PATH = "/patients/{" + PATIENT_ID + "}";
    String ON_PHOTO_PATH = ON_PATIENT_PATH + "/photo";
    String ON_PERSON_PATH = ON_PATIENT_PATH + "/person";
    String ON_PERSONS_PATH = "/persons";
}
