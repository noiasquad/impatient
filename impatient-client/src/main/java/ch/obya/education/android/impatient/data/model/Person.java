package ch.obya.education.android.impatient.data.model;


/**
 * Created by olivier on 05.11.15.
 */
public class Person {
    private String patientId;
    private String firstName;
    private String lastName;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String id) {
        this.patientId = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
