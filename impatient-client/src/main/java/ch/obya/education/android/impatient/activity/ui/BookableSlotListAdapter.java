package ch.obya.education.android.impatient.activity.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import ch.obya.education.android.impatient.data.model.BookableSlot;
import ch.obya.education.coursera.impatient.R;

public class BookableSlotListAdapter extends ArrayAdapter<BookableSlot> {
	
	public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

	public BookableSlotListAdapter(Context context) {
		super(context, R.layout.layout_bookable_slot_item);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final BookableSlot item = getItem(position);
		
		FrameLayout itemLayout = convertView == null ?
				(FrameLayout)LayoutInflater.from(getContext()).inflate(R.layout.layout_bookable_slot_item, null) :
					(FrameLayout)convertView;

		final TextView scheduleTime = (TextView) itemLayout.findViewById(R.id.schedule_time);
		scheduleTime.setText(DATE_FORMAT.format(item.getScheduleTime()));
		
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				makeReservation(item.getDay(), item.getSlotId());
			}
		});

		return itemLayout;
	}

	private void makeReservation(Date day, Byte slotId) {
	}
}
