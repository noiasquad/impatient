package ch.obya.education.android.impatient.operation;

import java.lang.ref.WeakReference;

import android.app.Activity;
import ch.obya.education.android.common.ConfigurableOps;
import ch.obya.education.android.common.GenericAsyncTask;
import ch.obya.education.android.common.GenericAsyncTaskOps;
import ch.obya.education.android.common.Utils;
import ch.obya.education.android.impatient.activity.HomeActivity;
import ch.obya.education.android.impatient.activity.LoginActivity;
import ch.obya.education.android.impatient.controller.AuthenticationController;
import ch.obya.education.android.impatient.util.Constants;

public class LoginOps implements ConfigurableOps {
	private static final String TAG = LoginOps.class.getSimpleName();

	private WeakReference<LoginActivity> activity;

	public LoginOps() {
	}

	public void onConfiguration(Activity activity, boolean firstTimeIn) {
		final String time = firstTimeIn ? "first time" : "second+ time";
		this.activity = new WeakReference<>((LoginActivity) activity);
	}

	public void authenticate(String username, String password) {
		new GenericAsyncTask<>(new GenericAsyncTaskOps<Object, Void, Boolean>() {
			@Override
			public Boolean doInBackground(Object... params) {
				String username = String.valueOf(params[0]);
				String password = String.valueOf(params[1]);
				String serverUrl = Constants.SERVER_URL;
				return AuthenticationController.authenticate(username, password, serverUrl);
			}

			@Override
			public void onPostExecute(Boolean result) {
				if (result)
					activity.get().startActivity(HomeActivity.openIntent(activity.get()));
				else
					Utils.showToast(activity.get(), "Authentication failed or maybe remote service is not available");
			}
		}).execute(username, password);
	}

    public void changeSecret(String oldSecret, String newSecret) {
        new GenericAsyncTask<>(new GenericAsyncTaskOps<Object, Void, Boolean>() {
            @Override
            public Boolean doInBackground(Object... params) {
                String oldSecret = String.valueOf(params[0]);
                String newSecret = String.valueOf(params[1]);
                return AuthenticationController.changeSecret(oldSecret, newSecret);
            }

            @Override
            public void onPostExecute(Boolean result) {
                if (result)
                    Utils.showToast(activity.get(), "Your password has been changed with success.");
                else
                    Utils.showToast(activity.get(), "Wrong password or maybe remote service is not available");
            }
        }).execute(oldSecret, newSecret);
    }

    private class CreateTask implements GenericAsyncTaskOps<Object, Void, String[]> {
        @Override
        public String[] doInBackground(Object... params) {
            return AuthenticationController.createUser();
        }

        @Override
        public void onPostExecute(String[] result) {
            if (result.length == 2)
                Utils.showToast(activity.get(), "Your new user account has been created successfully");
            else
                Utils.showToast(activity.get(), "Failure during user creation or maybe remote service is not available");
        }
    }
}
