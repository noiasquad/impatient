package ch.obya.education.android.impatient.data.model;

/**
 * Created by olivier on 05.11.15.
 */
public class MedicalRecord {
    private String patientId;
    private String firstname;
    private String lastname;
    private String address;
    private String zip;
    private String city;
    private String email;
}
