package ch.obya.education.android.impatient.activity;

import android.os.Bundle;
import ch.obya.education.android.common.GenericActivity;
import ch.obya.education.android.impatient.operation.SettingsOps;
import ch.obya.education.coursera.impatient.R;

public class SettingsActivity extends GenericActivity<SettingsOps> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_slot_list);
    }
}