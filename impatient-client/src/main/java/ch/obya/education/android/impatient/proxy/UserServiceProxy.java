package ch.obya.education.android.impatient.proxy;

import retrofit.http.GET;
import retrofit.http.POST;
import ch.obya.education.android.impatient.proxy.api.UserApi;

/**
 * Created by olivier on 08.11.15.
 */
public interface UserServiceProxy extends UserApi{

    @GET(USER_ENDPOINT_PATH + CURRENT_USER_ID_PATH)
    public String getCurrentUser();

    @POST(USER_ENDPOINT_PATH + ALL_USERS_PATH)
    public String[] createUser();

    @POST(USER_ENDPOINT_PATH + CURRENT_USER_SECRET_PATH)
    public void changeSecret(String oldSecret, String newSecret);
}
