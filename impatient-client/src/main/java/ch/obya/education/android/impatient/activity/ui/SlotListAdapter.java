package ch.obya.education.android.impatient.activity.ui;

import java.text.SimpleDateFormat;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import ch.obya.education.android.impatient.data.model.Slot;
import ch.obya.education.coursera.impatient.R;

public class SlotListAdapter extends ArrayAdapter<Slot> {
	
	public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

	public SlotListAdapter(Context context) {
		super(context, R.layout.layout_slot_list);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final Slot item = getItem(position);
		
		FrameLayout itemLayout = convertView == null ?
				(FrameLayout)LayoutInflater.from(getContext()).inflate(R.layout.layout_slot_item, null) :
					(FrameLayout)convertView;

		final TextView scheduleTime = (TextView) itemLayout.findViewById(R.id.schedule_time);
		scheduleTime.setText(DATE_FORMAT.format(item.getScheduleTime()));
		
		if (item.getPatientId() != null) {
	        final TextView patientName = (TextView) itemLayout.findViewById(R.id.patient_name);
	        patientName.setText(item.getPatientName());
			
			final ImageView patientPhoto = (ImageView) itemLayout.findViewById(R.id.patient_image_view);
		}
		
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			}
		});

		return itemLayout;
	}
}
