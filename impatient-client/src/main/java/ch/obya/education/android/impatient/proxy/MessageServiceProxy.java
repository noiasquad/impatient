package ch.obya.education.android.impatient.proxy;

import java.util.List;
import java.util.Map;

import retrofit.http.GET;
import ch.obya.education.android.impatient.proxy.api.MessageApi;

/**
 * Created by olivier on 05.11.15.
 */
public interface MessageServiceProxy extends MessageApi {
    @GET(MESSAGE_ENDPOINT_PATH + CURRENT_USER_MESSAGES_PATH)
    List<Map<String,String>> consume();
}
