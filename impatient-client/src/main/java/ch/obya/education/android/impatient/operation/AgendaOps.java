package ch.obya.education.android.impatient.operation;

import java.lang.ref.WeakReference;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import ch.obya.education.android.common.ConfigurableOps;
import ch.obya.education.android.common.GenericAsyncTask;
import ch.obya.education.android.common.GenericAsyncTaskOps;
import ch.obya.education.android.impatient.activity.AgendaActivity;
import ch.obya.education.android.impatient.data.model.Slot;
import ch.obya.education.android.impatient.proxy.ApiProxyInstance;

public class AgendaOps implements ConfigurableOps {

	/**
	 * Debugging tag used by the Android logger.
	 */
	private static final String TAG = AgendaOps.class.getSimpleName();

	/**
	 * Used to enable garbage collection.
	 */
	private WeakReference<AgendaActivity> activity;

	/**
	 * It allows access to application-specific resources.
	 */
	private Context applicationContext;

	/**
	 * Default constructor that's needed by the GenericActivity framework.
	 */
	public AgendaOps() {
	}

	/**
	 * Called after a runtime configuration change occurs to finish the initialization steps.
	 */
	public void onConfiguration(Activity activity, boolean firstTimeIn) {
		final String time = firstTimeIn ? "first time" : "second+ time";

		Log.d(TAG, "onConfiguration() called the " + time + " with activity = " + activity);

		this.activity = new WeakReference<>((AgendaActivity) activity);

		if (firstTimeIn) {
			applicationContext = activity.getApplicationContext();
			getTodayAgenda();
		}
	}

    public void getTodayAgenda() {
        new GenericAsyncTask<>(new GenericAsyncTaskOps<Void, Void, List<Slot>>() {
            @Override
            public List<Slot> doInBackground(Void... params) {
                return ApiProxyInstance.getInstance().getTodayAgenda();
            }

            @Override
            public void onPostExecute(List<Slot> result) {
            	activity.get().displayList(result);
            }
        }).execute();
    }

    public void getWeekAgenda() {
        new GenericAsyncTask<>(new GenericAsyncTaskOps<Void, Void, List<Slot>>() {
            @Override
            public List<Slot> doInBackground(Void... params) {
                return ApiProxyInstance.getInstance().getWeekAgenda();
            }

            @Override
            public void onPostExecute(List<Slot> result) {
            	activity.get().displayList(result);
            }
        }).execute();
    }

    public void getMonthAgenda() {
        new GenericAsyncTask<>(new GenericAsyncTaskOps<Void, Void, List<Slot>>() {
            @Override
            public List<Slot> doInBackground(Void... params) {
                return ApiProxyInstance.getInstance().getMonthAgenda();
            }

            @Override
            public void onPostExecute(List<Slot> result) {
            	activity.get().displayList(result);
            }
        }).execute();
    }
}
