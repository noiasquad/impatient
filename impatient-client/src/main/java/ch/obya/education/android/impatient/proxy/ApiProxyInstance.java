package ch.obya.education.android.impatient.proxy;

public class ApiProxyInstance {

    private static ApiProxy instance = null;

	private ApiProxyInstance() {}

    public static void setInstance(ApiProxy proxy) {
        instance = proxy;
    }

    public static ApiProxy getInstance() {
        return instance;
    }
}
